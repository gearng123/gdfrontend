import React , {Component} from 'react'
import styled from 'styled-components'
// import img from '../assets/images/web-designer.png'
import Icon from '@material-ui/core/Icon';

const Card = styled.div`
    width: 330px;
    height: 400px;
    box-shadow: 0 6px 20px 0 rgba(0, 0, 0, 0.19), 0 8px 17px 0 rgba(0, 0, 0, 0.2);
    background-color: #ffffff;
    margin: 0 10px 0 10px;
`
const CardHeader = styled.div`
    background-image: url(${require('../assets/images/web-designer.png')});
    background-size: cover;
    height: 50%;
    width: 100%;
`

const CardContent = styled.div`
    height: 30%;
    padding: 10px;
    overflow: scroll;
`

const Name = styled.a`
    font-weight: bold;
`

const Desciption = styled.p`
    word-wrap: break-word;
`
const Divider = styled.div`
    width: 100%
    height: 2px
    border: solid 1px #dddddd;
`

const CardFooter = styled.div`
    height: 10%;
    padding: 10px;
    display: flex;
    justify-content: flex-end
`

const View = styled.div`
    display: flex
    align-items: center
    p {
        color: gray;
        margin: 0 3px 0 3px;
    } 
`

const JobCard = ({jobName,jobDesciption,rating,views,...props}) => (
    <Card>
        <CardHeader/>
        <CardContent>
            <Name>{jobName}</Name>
            <Desciption>{jobDesciption}</Desciption>
        </CardContent>
        <Divider/>
        <CardFooter>
            <View>        
                <p>{rating}/5.0</p>
                <Icon style={{color:'#f5a623'}}>star</Icon>
            </View>
            <View>
                <p>{views}</p>
                <Icon style={{color:'gray'}}>visibility</Icon>
            </View>
        </CardFooter>
    </Card>
)

export default JobCard