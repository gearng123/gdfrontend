import  React,{Component} from 'react'
// import { Link } from 'react-router-dom'
import styled from 'styled-components'
import Button from '@material-ui/core/Button';

const Nav = styled.div`
    width: 100%
    height: 8vh
    display: flex
    justify-content: space-between
    align-items: center
`

const Logo = styled.a`
    display: inline-block;
    font-family: AmericanTypewriter
    font-size: 22px;
    font-weight: bold;
    margin-top: 10px;
    margin-left: 20px;
    color: white
    &:after {
        content: 'DeSINθ'
    }
`

const MainNav = styled.div`
    margin-right : 20px
    display: flex
    justify-content: center
    align-items: center
`
const LoginButton = styled.a`
    margin-right : 20px
    color: white
    text-align: center
    display: block
`

const Navbar = () => (
    <Nav>
        <Logo/>
        <MainNav>
            <LoginButton>เข้าสู่ระบบ</LoginButton>
            <Button variant="contained" color="secondary">สมัครสมาชิก</Button>
        </MainNav>
    </Nav>
)

export default Navbar