import React, { Component } from 'react';
import {BrowserRouter,Route,Switch} from 'react-router-dom';
import HomePage from './scenes/HomePage'

class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={HomePage}/>
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
