import React,{ Component } from 'react'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer'
import styled from 'styled-components'
import Button from '@material-ui/core/Button';
import JobCard from '../components/JobCard'
import bg from '../assets/images/hompage-bg.jpg'

const Layout = styled.div`
    width: 100%
    height: 100%
    display: flex
    flex-direction: column
    overflow: scroll
`
const BackGround = styled.div`
    position: absolute
    z-index: -8000
    width: 100%
    height: 100%
    background-size: cover
    background-image: url(${bg})
`

const Head = styled.section`
    display: flex
    flex-direction: column
    justify-content: center
    align-items: center
    height: 250px
    width: 100%
`

const ButtonBar = styled.div`
    display: flex
`

const Content = styled.section`
    background: white
    height: 400px
    padding: 10px
    display: flex
    align-items: center
    overflow-x: scroll;
`

const Logo = styled.a`
    display: inline-block;
    font-family: AmericanTypewriter
    font-size: 50px;
    font-weight: bold;
    margin-top: 10px;
    margin-left: 20px;
    color: white
    &:after {
        content: 'DeSINθ'
    }
`
const ButtonGroup = styled.div`
    display: flex
`

const StyledButton = styled(Button)`
    width: 100px
    background: linear-gradient(45deg, #fe6b8b 30%, #ff8e53 90%);
    &:hover {
        filter: brightness(0.9)
    }
`

const CardBox = styled.div`
    flex: 1;
    display: inline-flex;
    justify-content: center
`


const HomePage = () => (
        <Layout>
            <BackGround/>
            <Navbar/>
            <Head>
                <Logo/>
                <h1 style={{color:'white'}}>ศูนย์รวมดีไซเนอร์คุณภาพ ที่เหมาะกับคุณ</h1>
                <ButtonGroup>
                    <StyledButton variant="contained" color="primary">ค้นหางาน</StyledButton>
                    <div style={{width:'40px'}}/>
                    <StyledButton variant="contained" color="primary">จ้างงาน</StyledButton>
                </ButtonGroup>
            </Head>
            <Content>
                <CardBox>
                    <JobCard 
                        jobName='ออกแบบรูปเด็กๆ' 
                        jobDesciption='รูปเด็กน่ารักกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกasdรูปเด็กน่ารักกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกasdรูปเด็กน่ารักกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกasdรูปเด็กน่ารักกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกกasd'
                        views='3000'
                        rating='4.0'
                    />
                    <JobCard/>
                    <JobCard/>
                    <JobCard/>
                    <JobCard/>
                
                </CardBox>
            </Content>
            <div style={{height:'30px'}}/>
            <Footer/>
        </Layout>
    )
export default HomePage